import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

const CalculatorButton = (props) => {
    // console.log(props);
    return (
        <TouchableOpacity style={styles.button}
            onPress={() => props.onPressButton(props.buttonValue)}
        >
            <Text> {props.buttonValue} </Text>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({

    button: {
        flexBasis: "25%",
        justifyContent: "center",
        alignItems: "center",
    },

})



export default CalculatorButton;