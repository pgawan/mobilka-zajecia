import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';


const ListScreen = () => {

    const names = [
        { name: 'Imię 1', age: '20' },
        { name: 'Imię 2', age: '20' },
        { name: 'Imię 3', age: '20' },
        { name: 'Imię 4', age: '20' },
        { name: 'Imię 5', age: '20' },
        { name: 'Imię 6', age: '20' },
        { name: 'Imię 7', age: '20' },
        { name: 'Imię 8', age: '20' },
        { name: 'Imię 9', age: '20' },
        { name: 'Imię 10', age: '20' },

    ];

    return (
        <FlatList
            data={names}
            keyExtractor={(item) => (item.name)}
            renderItem={({ item }) => {
                return (
                    <Text style={styles.text}>Mam na imie {item.name} i mam {item.age} lat</Text>
                )

            }}
        />
    )
};

const styles = StyleSheet.create({
    text: {
        marginHorizontal: 50,
        marginVertical: 50,
    }
});

export default ListScreen;