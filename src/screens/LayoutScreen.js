import React from 'react';
import { View, Text, StyleSheet } from 'react-native';


const LayoutScreen = () => {

    return (
        <View style={styles.viewStyle}>
            <Text style={styles.child1Style} >Element 1 - child</Text>
            <Text style={styles.child2Style} >Element 2 - child</Text>
            <Text style={styles.child3Style} >Element 3 - child</Text>

        </View>
    )
};

const styles = StyleSheet.create({
    // textStyle: {
    //     borderColor: 'red',
    //     borderWidth: 1,
    //     fontSize: 20,
    //     margin: 3,
    //     width: 50,
    //     margin: 20,
    //     borderTopWidth: 20,
    //     borderLeftWidth: 20
    // },
    viewStyle: {
        borderColor: 'pink',
        borderWidth: 2,
        height: 200,
        justifyContent: "center",
        flexDirection: "row",
        alignItems: 'flex-start',
        // flexWrap: 'wrap'


    },
    child1Style: {
        flex: 2,
        flexBasis: 30,
        backgroundColor: "red"
    },
    child2Style: {
        flex: 2,
        backgroundColor: "blue",
        flexBasis: 30,
        alignSelf: "center"
    },
    child3Style: {
        flex: 2,
        backgroundColor: "green",
        flexBasis: 30,
    },
    child4Style: {
        flex: 4,
        backgroundColor: "yellow",
        flexBasis: 10,
    },

})


export default LayoutScreen; 