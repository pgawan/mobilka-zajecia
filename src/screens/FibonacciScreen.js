import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, FlatList } from 'react-native';


const StateScreen = () => {

    const [counter, setCounter] = useState(1);
    const [counter2, setCounter2] = useState(1);
    const [number, setNumber] = useState(1);
    const [table, setTable] = useState([]);
    return (
        <View>
            <Button
                title="+"
                onPress={
                    () => {

                        setCounter2(counter + counter2);
                        setCounter(counter2);
                        setNumber(number + 1);
                        setTable([...table, { fib: counter, key: number.toString() }]);
                    }
                }


            />

            <Text>fib{number}: {counter}</Text>
            <FlatList
                data={table}
                // keyExtractor={(item) => (item.fib)}
                renderItem={({ item }) => {
                    return (
                        <Text style={styles.text}>fib{item.key}: {item.fib}</Text>
                    )

                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({

});

export default StateScreen; 