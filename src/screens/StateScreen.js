import React, { useState } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';


const StateScreen = () => {
  // let counter = 0;
  const [counter, setCounter] = useState(0);
  return (
    <View>
      <Button
        title="+"
        onPress={
          () => {
            setCounter(counter + 1);
          }
        }


      />
      <Text>Counter: {counter} </Text>
    </View>
  );
};

const styles = StyleSheet.create({

});

export default StateScreen; 