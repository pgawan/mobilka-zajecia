import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity } from 'react-native';
import CalculatorButton from "../components/calculator/CalculatorButton";


const CalcScreen = () => {
    const [operationText, setOperationText] = useState('');
    const [result, setResult] = useState('0');

    const buttonPress = (buttonValue) => {
        switch (buttonValue) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                setOperationText(operationText + buttonValue)
                console.log(buttonValue);
                break;
            case '+':
            case '-':
            case '*':
            case '/':
                setOperationText(operationText + buttonValue);
                console.log(buttonValue);
                break;
            case '%':
                setOperationText(buttonValue + operationText / 100);
                break;
            case 'AC':
                console.log('clear');
                setResult([]);
                setOperationText([]);
                break;
            case 'Back':
                console.log('delate last');
                break;
            case '=':
                setResult(eval(operationText));
                console.log('operation:' + buttonValue);
                break;



        }
        // console.log(buttonValue);
        // 
    }

    return (

        <View style={styles.container}>
            <View style={styles.calculation}>
                <Text>{result} </Text>

            </View>
            <View style={styles.results}>
                <Text>{operationText} </Text>
            </View>
            <View style={styles.buttons}>
                <View style={styles.row}>

                    <CalculatorButton buttonValue={'AC'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'Back'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'%'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'/'} onPressButton={buttonPress} />

                </View>
                <View style={styles.row}>
                    <CalculatorButton buttonValue={'7'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'8'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'9'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'*'} onPressButton={buttonPress} />


                </View>
                <View style={styles.row}>
                    <CalculatorButton buttonValue={'4'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'5'} onPressButton={buttonPress} v />
                    <CalculatorButton buttonValue={'6'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'-'} onPressButton={buttonPress} />


                </View>
                <View style={styles.row}>
                    <CalculatorButton buttonValue={'1'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'2'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'3'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'+'} onPressButton={buttonPress} />


                </View>
                <View style={styles.row}>
                    <CalculatorButton buttonValue={''} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'0'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'.'} onPressButton={buttonPress} />
                    <CalculatorButton buttonValue={'='} onPressButton={buttonPress} />


                </View>
            </View>
        </View>



    )

};
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    results: {
        flex: 2,
        backgroundColor: 'green'
    },
    calculation: {
        flex: 1,
        backgroundColor: 'yellow'
    },
    buttons: {
        flex: 4,


    },
    row: {
        flex: 1,
        flexDirection: 'row',
    },
    button: {
        flexBasis: "25%",
        justifyContent: "center",
        alignItems: "center",
    },

})

export default CalcScreen;