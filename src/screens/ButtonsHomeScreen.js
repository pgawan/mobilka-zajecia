import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';


const ButtonsHomeScreen = ({ navigation }) => {

    return (

        <View>
            <Button
                title="Lista"
                color="blue"
                onPress={() => {
                    navigation.navigate('List');
                }}
            />
            <Button
                title="Home"
                color="red"
                onPress={() => {
                    navigation.navigate('Home');
                }}
            />
            <Button
                title="State"
                color="green"
                onPress={() => {
                    navigation.navigate('State');
                }}
            />
            <Button
                title="Fibonacci"
                color="black"
                onPress={() => {
                    navigation.navigate('Fibonacci');
                }}
            />
            {/* <Button
                title="Layout"
                color="purple"
                onPress={() => {
                    navigation.navigate('Layout');
                }}
            /> */}
            <Button
                title="MainLayout"
                color="pink"
                onPress={() => {
                    navigation.navigate('Layouts');
                }}
            />
            <Button
                title="Kalkulator"
                color="aqua"
                onPress={() => {
                    navigation.navigate('Calc');
                }}
            />
        </View>
    )

};

const styles = StyleSheet.create({
});

export default ButtonsHomeScreen; 