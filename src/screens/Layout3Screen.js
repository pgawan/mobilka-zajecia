import React from 'react';
import { View, Text, StyleSheet } from 'react-native';


const LayoutScreen3 = () => {

    return (
        <View style={styles.viewStyle}>
            <View style={styles.view1style}>
                <Text style={styles.child1Style} >Element 1 - child</Text>
                <Text style={styles.child2Style} >Element 2 - child</Text>
            </View>
            <View style={styles.view2style}>
                <Text style={styles.child3Style} >Element 3 - child</Text>
            </View>
            <View style={styles.view3style}>
                <Text style={styles.child4Style} >Element 4 - child</Text>
                <Text style={styles.child5Style} >Element 5 - child</Text>
            </View>

        </View>
    )
};

const styles = StyleSheet.create({

    viewStyle: {
        borderColor: 'pink',
        borderWidth: 2,
        height: 200,
        flexDirection: "row",
        justifyContent: "center"
    },
    view1style: {


        justifyContent: "space-between"
    },
    view2style: {


        justifyContent: "center"
    },
    view3style: {


        justifyContent: "space-between"
    },
    child1Style: {
        width: 60,
        height: 60,
        backgroundColor: "red",

    },
    child2Style: {
        width: 60,
        height: 60,
        backgroundColor: "blue",


    },
    child3Style: {
        width: 60,
        height: 60,
        backgroundColor: "green",


    },
    child4Style: {
        width: 60,
        height: 60,
        backgroundColor: "purple",

    },
    child5Style: {
        width: 60,
        height: 60,
        backgroundColor: "pink",
    },


})


export default LayoutScreen3; 