import React from 'react';
import { View, Text, StyleSheet } from 'react-native';


const LayoutScreen2 = () => {

    return (
        <View style={styles.viewStyle}>
            <Text style={styles.child1Style} >Element 1 - child</Text>
            <Text style={styles.child2Style} >Element 2 - child</Text>
            <Text style={styles.child3Style} >Element 3 - child</Text>

        </View>
    )
};

const styles = StyleSheet.create({

    viewStyle: {
        borderColor: 'pink',
        borderWidth: 2,
        height: 200,
        justifyContent: "center",
        flexDirection: "row",
        alignItems: 'flex-start',
        // flexWrap: 'wrap'


    },
    child1Style: {
        flex: 2,
        flexBasis: 30,
        backgroundColor: "red"
    },
    child2Style: {
        flex: 2,
        backgroundColor: "blue",
        flexBasis: 30,
        alignSelf: "center"
    },
    child3Style: {
        flex: 2,
        backgroundColor: "green",
        flexBasis: 30,
        alignSelf: 'flex-end',
    },


})


export default LayoutScreen2; 