import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

const MainLayoutScreen = ({ navigation }) => {

    return (

        <View>
            <Button
                title="Cwiczenie1"
                color="blue"
                onPress={() => {
                    navigation.navigate('Layout');
                }}
            />
            <Button
                title="Cwiczenie2"
                color="red"
                onPress={() => {
                    navigation.navigate('Layout2');
                }}
            />
            <Button
                title="Cwiczenie3"
                color="green"
                onPress={() => {
                    navigation.navigate('Layout3');
                }}
            />

        </View>
    )
}

export default MainLayoutScreen;