import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/screens/HomeScreen';
import ListScreen from './src/screens/ListScreen';
import ButtonsHomeScreen from './src/screens/ButtonsHomeScreen';
import StateScreen from './src/screens/StateScreen';
import FibonacciScreen from './src/screens/FibonacciScreen';
import LayoutScreen from './src/screens/LayoutScreen';
import Layout2Screen from './src/screens/Layout2Screen';
import Layout3Screen from './src/screens/Layout3Screen';
import MainLayoutScreen from './src/screens/MainLayoutScreen';
import CalcScreen from './src/screens/CalcScreen';

const MainNavigator = createStackNavigator({
  Home: { screen: HomeScreen },
  List: { screen: ListScreen },
  ButtonsHome: { screen: ButtonsHomeScreen },
  State: { screen: StateScreen },
  Fibonacci: { screen: FibonacciScreen },
  Layouts: { screen: MainLayoutScreen },
  Layout: { screen: LayoutScreen },
  Layout2: { screen: Layout2Screen },
  Layout3: { screen: Layout3Screen },
  Calc: { screen: CalcScreen },

},
  {
    initialRouteName: 'ButtonsHome',
    defaultNavigationOptions: {
      title: 'Start'
    }
  }
)

export default createAppContainer(MainNavigator);


